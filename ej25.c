#include <stdio.h>

float power(float b, int e) {
	if (e == 0)
		return 1;

	if (e < 0)
		return 1.0f/b * power(b, e + 1);
	else
		return b * power(b, e - 1);
}

int main() {
	float b;
	int e;

	printf("Introduce la base: ");
	scanf("%f", &b);
	printf("Introduce el exponente: ");
	scanf("%d", &e);

	printf("%f^%d = %f\n", b, e, power(b, e));

	return 0;
}
