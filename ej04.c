#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool foo(float a, float b, float c, float *sol1, float *sol2) {
	float aux;

	aux = powf(b, 2) - 4 * a * c;
	if (aux < 0)
		return false;

	*sol1 = (-b + sqrtf(aux))/ (2*a);
	*sol2 = (-b - sqrtf(aux))/ (2*a);

	return true;
}

int main() {
	float a, b, c;
	float s1, s2;

	printf("Introduce el valor de a: ");
	scanf("%f", &a);

	printf("Introduce el valor de b: ");
	scanf("%f", &b);

	printf("Introduce el valor de c: ");
	scanf("%f", &c);

	if (foo(a, b, c, &s1, &s2))
		printf("Las soluciones son %f y %f\n", s1, s2);
	else
		printf("No se puede resolver con numeros reales\n");


	return 0;
}

