#include <stdio.h>
#include <math.h>
#include <stdbool.h>


bool is_prime(int n) {
	int cnt;

	if (n < 0)
		n = -n;

	for (cnt = sqrtf(n); cnt > 1; cnt--)
		if (n % cnt == 0)
			return false;

	return true;
}

int main() {
	int n;

	printf("Introduce un numero para indicar si es primo o no: ");
	scanf("%d", &n);

	printf("%d%s es primo\n", n, is_prime(n)? "": " no");

	return 0;
}

