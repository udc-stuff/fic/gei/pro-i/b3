#include <stdio.h>

int foo(int bar) {
	int cnt;

	if (bar < 2)
		return -1;

	for (cnt = bar / 2; cnt > 1; cnt--)
		if (bar % cnt == 0)
			return cnt;

	return cnt; 	
}


int main() {
	int n, aux;

	printf("Introduce un numero entero positivo: ");
	scanf("%d", &n);

	aux = foo(n);

	if (aux == -1)
		printf("%d: numero invalido\n", n);
	else
		printf("El mayor divisor de %d es %d\n", n, aux);

	return 0;
}
