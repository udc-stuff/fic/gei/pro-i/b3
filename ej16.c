#include <stdio.h>
#include <stdbool.h>

bool is_number(char c) {
	return c >= '0' && c <= '9';
}

int main() {
	char c;

	printf("Introduce un caracter: ");
	scanf("%c", &c);

	printf("%c %s un numero\n", c, is_number(c)?"es":"no es");

	return 0;
}
