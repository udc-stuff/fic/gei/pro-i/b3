#include <stdio.h>
#include <math.h>

int len(int number) {
	int cnt = 0;

	if (number == 0)
		return 1;

	while (number != 0) {
		number /= 10;
		cnt++;
	}

	return cnt;
}

int reverse(int number, int pos) {
	if (number < 10)
		return number;
	else
		return number % 10 * powf(10, pos) + reverse(number / 10, pos - 1);
}

int main() {
	int n;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	printf("%d\n%d\n", n, reverse(n, len(n) - 1));

	return 0;
}
