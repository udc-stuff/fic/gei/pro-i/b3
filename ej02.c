#include <stdio.h>

void print_day(char c) {
	switch (c) {
		case 'L':
		case 'l':
			printf("Lunes");
			break;
		case 'M':
		case 'm':
			printf("Martes");
			break;
		case 'X':
		case 'x':
			printf("Miercoles");
			break;
		case 'J':
		case 'j':
			printf("Jueves");
			break;
		case 'V':
		case 'v':
			printf("Viernes");
			break;
		case 'S':
		case 's':
			printf("Sabado");
			break;
		case 'D':
		case 'd':
			printf("Domingo");
			break;
		default:
			printf("[Valor incorrecto]");
	}
}

int main() {
	char c;

	printf("Introduce uno de los identificadores de la lista (L, M, X, J, V, S, D): ");
	scanf("%c", &c);

	print_day(c);
	printf("\n");

	return 0;
}
