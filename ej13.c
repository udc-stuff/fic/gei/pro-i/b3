#include <stdio.h>
#include <math.h>

float foo(float x) {
	return 3*powf(x, 5) + 2*powf(x, 4) - 5*powf(x,3) - powf(x, 2) + 7*x - 6;
}

int main() {
	float x;

	printf("Introduce un numero para calcular el polinomio: ");
	scanf("%f", &x);

	printf("El valor del polinomio es %f\n", foo(x));

	return 0;
}
