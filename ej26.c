#include <stdio.h>

int foo(int bar) {
	if (bar <= 1)
		return 1;

	return bar + foo(bar - 1);
}

int main() {
	int n;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	printf("La suma de los primeros %d numeros es %d\n", n, foo(n));

	return 0;
}
