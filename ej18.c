#include <stdio.h>
#include <stdbool.h>

bool is_number(char c) {
	return c >= '0' && c <= '9';
}

bool is_upper(char c) {
	return c >= 'A' && c <= 'Z';
}

bool is_lower(char c) {
	return c >= 'a' && c <= 'z';
}

int main() {
	char s[200];
	int cnt, digit, upper, lower;

	printf("Introduzca un cadena: ");
	scanf("%s", s);

	for (digit = upper = lower = cnt = 0; s[cnt] != '\0'; cnt++) {
		if (is_number(s[cnt]))
			digit++;
		else if (is_upper(s[cnt]))
			upper++;
		else if (is_lower(s[cnt]))
			lower++;
	}

	printf("%s tiene %d mayusculas, %d minusculas y %d numeros\n", s, upper, lower, digit);

	return 0;
}
