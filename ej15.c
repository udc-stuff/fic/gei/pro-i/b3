#include <stdio.h>
#include <stdbool.h>

bool EsVocal(char c) {
	switch (c) {
		case 'a':
		case 'A':
		case 'e':
		case 'E':
		case 'i':
		case 'I':
		case 'o':
		case 'O':
		case 'u':
		case 'U':
			return true;
	}
	return false;
}

int main() {
	char c;

	printf("Introduce un caracter: ");
	scanf("%c", &c);

	printf("%c %s vocal\n", c, EsVocal(c)?"es":"no es");

	return 0;
}
