#include <stdio.h>

char to_upper(char c) {
	if (c < 'a' || c > 'z')
		return c;
	else
		return c - ('a' - 'A');
}

int main() {
	char s[50];
	int cnt;

	printf("Introduce una cadena: ");
	scanf("%s", s);
;
	for (cnt = 0; s[cnt] != '\0'; cnt++)
		printf("%c", to_upper(s[cnt]));

	printf("\n");
	return 0;
}
