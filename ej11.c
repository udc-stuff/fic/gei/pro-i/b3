#include <stdio.h>

int get_int_between_range(int min, int max) {
	int n = min -1;

	while (n < min || n > max) {
		printf("Introduce un numero entre %d y %d: ", min, max);
		scanf("%d", &n);
	}

	return n;
}

int main() {
	int n;

	n = get_int_between_range(-10, 10);

	printf("El numero introducido es %d\n", n);

	return 0;

}
