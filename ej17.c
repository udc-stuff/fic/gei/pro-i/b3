#include <stdio.h>
#include <stdbool.h>

bool is_upper(char c) {
	return c >= 'A' && c <= 'Z';
}

int main() {
	char c;

	printf("Introduzca un caracter: ");
	scanf("%c", &c);

	if (is_upper(c))
		printf("%c es un caracter en mayusculas\n", c);
	else
		printf("%c no es un caracter en mayusculas\n", c);

	return 0;
}
