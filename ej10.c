#include <stdio.h>

char foo(int bar) {
	return bar < 0? 'N': 'P';
}

int main() {
	int n;

	printf("Introduce un numero entero: ");
	scanf("%d", &n);

	printf("%d es %c\n", n, foo(n));

	return 0;
}
