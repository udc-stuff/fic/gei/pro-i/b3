#include <stdio.h>

int MesSiguiente(int mes) {
	return mes % 12 + 1;
}

int main() {
	int month;

	printf("Introduce un numero de mes: ");
	scanf("%d", &month);

	printf("El siguiente mes a %d es %d\n", month, MesSiguiente(month));

	return 0;
}
