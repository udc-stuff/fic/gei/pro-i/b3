#include <stdio.h>

int fibonacci(int n) {
	int cnt, n1, n2, n3;
	if (n < 0)
		return -1;

	if (n == 0 || n == 1)
		return n;

	n1 = 0;
	n2 = 1;
	for (cnt = 2; cnt <= n; cnt++) {
		n3 = n2 + n1;
		n1 = n2;
		n2 = n3;
	}

	return n3;
}

int main() {
	int n;

	printf("Introduce el termino a calcular de la serie de fibonacci: ");
	scanf("%d", &n);

	printf("El termino %d de fibonacci es %d\n", n, fibonacci(n));

	return 0;
}
