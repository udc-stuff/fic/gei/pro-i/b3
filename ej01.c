#include <stdio.h>

void foo(int lines) {
	int cnt;
	for (cnt = 0; cnt < lines; cnt++)
		printf("\n");
}

int main() {
	int n;

	printf("Introduce el numero de lineas en blanco a mostrar: ");
	scanf("%d", &n);

	foo(n);

	return 0;
}
