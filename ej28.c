#include <stdio.h>

void table(int base, int cnt) {
	if (cnt < 1)
		return;

	table(base, cnt - 1);
	printf("%d x %d = %d\n", base, cnt, base * cnt);
}

int main() {
	int base, cnt;

	printf("Introduzca el numero entero: ");
	scanf("%d", &base);
	printf("Introduzca el maximo multiplicador: ");
	scanf("%d", &cnt);

	table(base, cnt);

	return 0;
}

