#include <stdio.h>

int fact(int n) {
	int tmp, cnt;

	tmp = 1;
	for (cnt = 1; cnt <= n; cnt++)
		tmp *= cnt;

	return tmp;
}

int main() {
	int n;

	printf("Introduce un numero para calcualr su factorial: ");
	scanf("%d", &n);

	printf("%d! = %d\n" ,n, fact(n));

	return 0;
}

