#include <stdio.h>

long fact_it(long n) {
	long total, cnt;

	if (n < 0)
		return -1;

	total = 1;
	for (cnt = 2; cnt <= n; cnt++)
		total *= cnt;

	return total;
}

long fact_rec(long n) {
	if (n < 0)
		return -1;
	else if (n == 0)
		return 1;

	return n * fact_rec(n -1);
}


int main() {
	long n;

	printf("Introduce un numero para calcular su factorial: ");
	scanf("%ld", &n);

	printf("%ld! = %ld (version iterativa)\n", n, fact_it(n));
	printf("%ld! = %ld (version recursiva)\n", n, fact_rec(n));

	return 0;
}
