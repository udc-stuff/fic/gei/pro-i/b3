#include <stdio.h>

int foo(int bar) {
	if (bar < 10)
		return bar;

	return bar % 10 + foo(bar / 10);
}

int main() {
	int n;

	printf("Introduce un numero entero: ");
	scanf("%d", &n);

	printf("La suma de las cifras de %d es %d\n", n, foo(n));

	return 0;
}
