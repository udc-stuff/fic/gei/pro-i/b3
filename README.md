En este repositorio os encontraréis ejercicios del boletín 3 que se han realizado en clase de prácticas.

Quizás encontréis alguna pequeña modificación con respecto al enunciado. Recordar que esas pequeñas modificaciones han sido realizadas para explicar cosas concretas que ocurren al usar el lenguaje C.

# Ej 01.

```c
#include <stdio.h>

void foo(int lines) {
	int cnt;
	for (cnt = 0; cnt < lines; cnt++)
		printf("\n");
}

int main() {
	int n;

	printf("Introduce el numero de lineas en blanco a mostrar: ");
	scanf("%d", &n);

	foo(n);

	return 0;
}
```

# Ej 02.

```c
#include <stdio.h>

void print_day(char c) {
	switch (c) {
		case 'L':
		case 'l':
			printf("Lunes");
			break;
		case 'M':
		case 'm':
			printf("Martes");
			break;
		case 'X':
		case 'x':
			printf("Miercoles");
			break;
		case 'J':
		case 'j':
			printf("Jueves");
			break;
		case 'V':
		case 'v':
			printf("Viernes");
			break;
		case 'S':
		case 's':
			printf("Sabado");
			break;
		case 'D':
		case 'd':
			printf("Domingo");
			break;
		default:
			printf("[Valor incorrecto]");
	}
}

int main() {
	char c;

	printf("Introduce uno de los identificadores de la lista (L, M, X, J, V, S, D): ");
	scanf("%c", &c);

	print_day(c);
	printf("\n");

	return 0;
}
```

# Ej 03.

```c
#include <stdio.h>

int foo(int bar) {
	int cnt;

	if (bar < 2)
		return -1;

	for (cnt = bar / 2; cnt > 1; cnt--)
		if (bar % cnt == 0)
			return cnt;

	return cnt; 	
}


int main() {
	int n, aux;

	printf("Introduce un numero entero positivo: ");
	scanf("%d", &n);

	aux = foo(n);

	if (aux == -1)
		printf("%d: numero invalido\n", n);
	else
		printf("El mayor divisor de %d es %d\n", n, aux);

	return 0;
}
```

# Ej 04.

```c
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool foo(float a, float b, float c, float *sol1, float *sol2) {
	float aux;

	aux = powf(b, 2) - 4 * a * c;
	if (aux < 0)
		return false;

	*sol1 = (-b + sqrtf(aux))/ (2*a);
	*sol2 = (-b - sqrtf(aux))/ (2*a);

	return true;
}

int main() {
	float a, b, c;
	float s1, s2;

	printf("Introduce el valor de a: ");
	scanf("%f", &a);

	printf("Introduce el valor de b: ");
	scanf("%f", &b);

	printf("Introduce el valor de c: ");
	scanf("%f", &c);

	if (foo(a, b, c, &s1, &s2))
		printf("Las soluciones son %f y %f\n", s1, s2);
	else
		printf("No se puede resolver con numeros reales\n");


	return 0;
}

```

# Ej 05.

```c
#include <stdio.h>

void floyd(int n) {
	int cnt, row, col;

	for (cnt = row = 1; cnt <= n; row++) {
		for (col = 1; col <= row && cnt<= n; col++) {
			printf("%d\t", cnt);
			cnt++;
		} 
		printf("\n");
	}

}


int main() {
	int n;

	printf("Introduce el numero donde parar: ");
        scanf("%d", &n);

	floyd(n);

	return 0;
}
```

# Ej 06.

```c
#include <stdio.h>

void floyd(int n) {
	int cnt, row, col;

	for (cnt = row = 1; row <= n; row++) {
		for (col = 1; col <= row; col++) {
			printf("%d\t", cnt);
			cnt++;
		} 
		printf("\n");
	}

}

int main() {
	int n;

	printf("Introduce el numero de filas: ");
        scanf("%d", &n);

	floyd(n);

	return 0;
}
```

# Ej 07.

```c
#include <stdio.h>
#include <stdbool.h>

bool check_date(int d, int m, int y) {
	if (d < 1 || m < 1 || y < 1)
		return false;
	
	if (m > 12)
		return false;

	switch (m) {
		case 2:
			if (y % 4 == 0 && (y % 100 != 0 || y % 400 == 0))
				return d <= 29;
			else
				return d <= 28;
		case 4:
		case 6:
		case 9:
		case 11:
			return d <= 30;
		default:
			return d <= 31;
	}	
}

int main() {
	int day, month, year;

	printf("Introduzca una fecha (dd/mm/yyyy): ");
	scanf("%d/%d/%d", &day, &month, &year);

	printf("%02d/%02d/%04d es %s\n", day, month, year, check_date(day, month, year) ? "valida": "invalida");

	return 0;
}
```

# Ej 08.

```c
#include <stdio.h>

int MesSiguiente(int mes) {
	return mes % 12 + 1;
}

int main() {
	int month;

	printf("Introduce un numero de mes: ");
	scanf("%d", &month);

	printf("El siguiente mes a %d es %d\n", month, MesSiguiente(month));

	return 0;
}
```

# Ej 10.

```c
#include <stdio.h>

char foo(int bar) {
	return bar < 0? 'N': 'P';
}

int main() {
	int n;

	printf("Introduce un numero entero: ");
	scanf("%d", &n);

	printf("%d es %c\n", n, foo(n));

	return 0;
}
```

# Ej 11.

```c
#include <stdio.h>

int get_int_between_range(int min, int max) {
	int n = min -1;

	while (n < min || n > max) {
		printf("Introduce un numero entre %d y %d: ", min, max);
		scanf("%d", &n);
	}

	return n;
}

int main() {
	int n;

	n = get_int_between_range(-10, 10);

	printf("El numero introducido es %d\n", n);

	return 0;

}
```

# Ej 12.

```c
#include <stdio.h>

int foo(int bar) {
	int cnt, total;

	for (cnt = total = 0; cnt <= bar; cnt++)
		total += cnt;

	return total;
}

int main() {
	int n;

	printf("Introduce un numeor entero: ");
	scanf("%d", &n);

	printf("La suma desde 1 hasta %d es %d\n", n, foo(n));

	return 0;
}
```

# Ej 13.

```c
#include <stdio.h>
#include <math.h>

float foo(float x) {
	return 3*powf(x, 5) + 2*powf(x, 4) - 5*powf(x,3) - powf(x, 2) + 7*x - 6;
}

int main() {
	float x;

	printf("Introduce un numero para calcular el polinomio: ");
	scanf("%f", &x);

	printf("El valor del polinomio es %f\n", foo(x));

	return 0;
}
```

# Ej 15.

```c
#include <stdio.h>
#include <stdbool.h>

bool EsVocal(char c) {
	switch (c) {
		case 'a':
		case 'A':
		case 'e':
		case 'E':
		case 'i':
		case 'I':
		case 'o':
		case 'O':
		case 'u':
		case 'U':
			return true;
	}
	return false;
}

int main() {
	char c;

	printf("Introduce un caracter: ");
	scanf("%c", &c);

	printf("%c %s vocal\n", c, EsVocal(c)?"es":"no es");

	return 0;
}
```

# Ej 16.

```c
#include <stdio.h>
#include <stdbool.h>

bool is_number(char c) {
	return c >= '0' && c <= '9';
}

int main() {
	char c;

	printf("Introduce un caracter: ");
	scanf("%c", &c);

	printf("%c %s un numero\n", c, is_number(c)?"es":"no es");

	return 0;
}
```

# Ej 17.

```c
#include <stdio.h>
#include <stdbool.h>

bool is_upper(char c) {
	return c >= 'A' && c <= 'Z';
}

int main() {
	char c;

	printf("Introduzca un caracter: ");
	scanf("%c", &c);

	if (is_upper(c))
		printf("%c es un caracter en mayusculas\n", c);
	else
		printf("%c no es un caracter en mayusculas\n", c);

	return 0;
}
```

# Ej 18.

```c
#include <stdio.h>
#include <stdbool.h>

bool is_number(char c) {
	return c >= '0' && c <= '9';
}

bool is_upper(char c) {
	return c >= 'A' && c <= 'Z';
}

bool is_lower(char c) {
	return c >= 'a' && c <= 'z';
}

int main() {
	char s[200];
	int cnt, digit, upper, lower;

	printf("Introduzca un cadena: ");
	scanf("%s", s);

	for (digit = upper = lower = cnt = 0; s[cnt] != '\0'; cnt++) {
		if (is_number(s[cnt]))
			digit++;
		else if (is_upper(s[cnt]))
			upper++;
		else if (is_lower(s[cnt]))
			lower++;
	}

	printf("%s tiene %d mayusculas, %d minusculas y %d numeros\n", s, upper, lower, digit);

	return 0;
}
```

# Ej 19.

```c
#include <stdio.h>

char to_upper(char c) {
	if (c < 'a' || c > 'z')
		return c;
	else
		return c - ('a' - 'A');
}

int main() {
	char s[50];
	int cnt;

	printf("Introduce una cadena: ");
	scanf("%s", s);
;
	for (cnt = 0; s[cnt] != '\0'; cnt++)
		printf("%c", to_upper(s[cnt]));

	printf("\n");
	return 0;
}
```

# Ej 20.

```c
#include <stdio.h>

int fact(int n) {
	int tmp, cnt;

	tmp = 1;
	for (cnt = 1; cnt <= n; cnt++)
		tmp *= cnt;

	return tmp;
}

int main() {
	int n;

	printf("Introduce un numero para calcualr su factorial: ");
	scanf("%d", &n);

	printf("%d! = %d\n" ,n, fact(n));

	return 0;
}

```

# Ej 21.

```c
#include <stdio.h>

int fibonacci(int n) {
	int cnt, n1, n2, n3;
	if (n < 0)
		return -1;

	if (n == 0 || n == 1)
		return n;

	n1 = 0;
	n2 = 1;
	for (cnt = 2; cnt <= n; cnt++) {
		n3 = n2 + n1;
		n1 = n2;
		n2 = n3;
	}

	return n3;
}

int main() {
	int n;

	printf("Introduce el termino a calcular de la serie de fibonacci: ");
	scanf("%d", &n);

	printf("El termino %d de fibonacci es %d\n", n, fibonacci(n));

	return 0;
}
```

# Ej 22.

```c
#include <stdio.h>
#include <math.h>
#include <stdbool.h>


bool is_prime(int n) {
	int cnt;

	if (n < 0)
		n = -n;

	for (cnt = sqrtf(n); cnt > 1; cnt--)
		if (n % cnt == 0)
			return false;

	return true;
}

int main() {
	int n;

	printf("Introduce un numero para indicar si es primo o no: ");
	scanf("%d", &n);

	printf("%d%s es primo\n", n, is_prime(n)? "": " no");

	return 0;
}

```

# Ej 24.

```c
#include <stdio.h>

long fact(long n) {
	long total, cnt;

	if (n < 0)
		return -1;

	total = 1;
	for (cnt = 2; cnt <= n; cnt++)
		total *= cnt;

	return total;
}

int main() {
	long n;

	printf("Introduce un numero para calcular su factorial: ");
	scanf("%ld", &n);

	printf("%ld! = %ld\n", n, fact(n));

	return 0;
}
```

# Ej 25.

```c
#include <stdio.h>

float power(float b, int e) {
	if (e == 0)
		return 1;

	if (e < 0)
		return 1.0f/b * power(b, e + 1);
	else
		return b * power(b, e - 1);
}

int main() {
	float b;
	int e;

	printf("Introduce la base: ");
	scanf("%f", &b);
	printf("Introduce el exponente: ");
	scanf("%d", &e);

	printf("%f^%d = %f\n", b, e, power(b, e));

	return 0;
}
```

# Ej 26.

```c
#include <stdio.h>

int foo(int bar) {
	if (bar <= 1)
		return 1;

	return bar + foo(bar - 1);
}

int main() {
	int n;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	printf("La suma de los primeros %d numeros es %d\n", n, foo(n));

	return 0;
}
```

# Ej 27.

```c
#include <stdio.h>
#include <math.h>

int len(int number) {
	int cnt = 0;

	if (number == 0)
		return 1;

	while (number != 0) {
		number /= 10;
		cnt++;
	}

	return cnt;
}

int reverse(int number, int pos) {
	if (number < 10)
		return number;
	else
		return number % 10 * powf(10, pos) + reverse(number / 10, pos - 1);
}

int main() {
	int n;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	printf("%d\n%d\n", n, reverse(n, len(n) - 1));

	return 0;
}
```

# Ej 28.

```c
#include <stdio.h>

void table(int base, int cnt) {
	if (cnt < 1)
		return;

	table(base, cnt - 1);
	printf("%d x %d = %d\n", base, cnt, base * cnt);
}

int main() {
	int base, cnt;

	printf("Introduzca el numero entero: ");
	scanf("%d", &base);
	printf("Introduzca el maximo multiplicador: ");
	scanf("%d", &cnt);

	table(base, cnt);

	return 0;
}

```

# Ej 29.

```c
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int len(int number) {
	int cnt = 0;

	if (number == 0)
		return 1;

	while (number != 0) {
		number /= 10;
		cnt++;
	}

	return cnt;
}

int reverse(int number, int pos) {
	if (number < 10)
		return number;
	else
		return number % 10 * powf(10, pos) + reverse(number / 10, pos - 1);
}

bool is_palindromic(int number) {
	return number == reverse(number, len(number) - 1);
}

int main() {
	int n;

	printf("Introduce un numero: ");
	scanf("%d", &n);

	printf("%d %s capicua\n", n, is_palindromic(n)? "es": "no es");

	return 0;
}
```

# Ej 30.

```c
#include <stdio.h>

int foo(int bar) {
	if (bar < 10)
		return bar;

	return bar % 10 + foo(bar / 10);
}

int main() {
	int n;

	printf("Introduce un numero entero: ");
	scanf("%d", &n);

	printf("La suma de las cifras de %d es %d\n", n, foo(n));

	return 0;
}
```
